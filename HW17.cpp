﻿#include <iostream>

using namespace std;


class Vector 
{

private:
    double x = 0;
    double y = 0;
    double z = 0;


public:
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    
    void ShowData()
    {
        cout << "\nYour Vector is " << x << " " << y << " " << z << endl;
    }
    
    double RangeOfVector()
    {
        double RangeResult = sqrt((x*x)+(y*y)+(z*z));
        return RangeResult; 
    }
};


int main()
{
    double X, Y, Z;
    cout << "Please enter X for your Vector" << endl;
    cin >> X;
    cout << "Please enter Y for your Vector" << endl;
    cin >> Y;
    cout << "Please enter Z for your Vector" << endl;
    cin >> Z;

    Vector UserVector(X, Y, Z);
    UserVector.ShowData();
    cout << "Your Vector Range = " << UserVector.RangeOfVector();





}

